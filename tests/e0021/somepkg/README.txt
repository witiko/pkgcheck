% Bourbaki (Brochure/Astérisque)
\NeedsTeXFormat{LaTeX2e}% LaTeX June 94 or later required
\def\batchfile{smflatex.ins}
\input docstrip.tex
\preamble
\endpreamble

\keepsilent

\csname newtoks\endcsname\filesmsg
\def\dofile#1#2{\generateFile{#1}{}{\from{smflatex.dtx}{#2}}%
  \filesmsg\expandafter{\the\filesmsg
    \Msg{* \space\space#1}}}

%  Classes smfbook et smfart
\dofile{smfbook.cls}{book}
\dofile{smfart.cls}{article}

% Package d'adaptations pour hyperref
\dofile{smfhyperref.sty}{hyperref}

%  Package de theoremes
\dofile{smfthm.sty}{smfthm}

% Package pour la ponctuation haute
\dofile{upitparen.sty}{upitparen}

% Package de listes
\dofile{smfenum.sty}{smfenum}

%  Fichiers de configuration SMF
\dofile{smfgen.sty}{smfgen}

\dofile{asterisque.sty}{asterisque}
\dofile{bulletin.sty}{bulletin}
\dofile{bull.sty}{bull}
\dofile{courspe.sty}{courspe}
\dofile{courspeedp.sty}{courspeedp}
\dofile{documents.sty}{documents}
\dofile{memoires.sty}{memoires}
\dofile{panoramas.sty}{panoramas}
\dofile{seminaires.sty}{seminaires}

\dofile{bourbaki.cls}{asterki}

%  Package multi
\dofile{smfmulti.sty}{multi}

% Package smfbib
\dofile{smfbib.sty}{smfbib}
% ki (Brochure/Astérisque)

%  Source commente des macros
\dofile{smfdoc.tex}{doc}

\endinput
