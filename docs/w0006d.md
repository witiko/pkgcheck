## W0006 -- Very large file with size `<size>` detected in TDS zip archive

(Experimental) We issue the message if there is a file larger than 40MiB
in the TDS zip archive.
