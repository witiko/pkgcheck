
## E0030 -- A symlink was found which points outside of the package directory tree

A symlink must not point to a file or directory outside of the package directory tree.
