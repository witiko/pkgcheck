#! /bin/sh

#
# get version identifier from Cargo.toml
#

version=$( perl -wln -e '/^version\s=\s"(.*)"/ and print $1;' ../Cargo.toml )


echo "\title{pkgcheck Utility, v$version}" > title.tex
