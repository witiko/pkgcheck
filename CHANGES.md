# General remarks

- the x.y.z is the version of the Rust source code which follows semantic versioning
- the combination of version and version date designates the version of the package as it is uploaded to CTAN


# Changes

2018-12-09 (1.0.0)
   - 1.0.0   First stable version

2018-12-16 (1.0.0)
   - quick_intro.txt: improve wording
   - build_ctan_zip.p6: add source files to be included into CTAN zip archive
   - devnotes.md: add documentation how to build your own binary

2018-12-26 (1.1.0)
   - enhance check for generated files which now could reside in any
     subdirectory in the package tree. This changes the format of the
     e0019 message slightly where now the path of a generated file will be
     displayed in the e0019 message
   - change ordering of the sections in the pkgcheck.pdf document. First come the
     informational messages, then warnings and error messages and finally fatal messages.
   - add a short info to the PDF documention about how to install pkgcheck

2019-01-02 (1.2.0)
   - some code simplification
   - **New feature**: checks that path names in the TDS zip archive contain the package name
     (only exception is a man page path)
   - add test cases for messages e0026, e0028

2019-03-09 (1.3.0)
   - recompile with newest http library
   - checking URLs
     - when checking URLs we try to get the headers first. If this fails we try to get the web page.
     - when checking URLs and a redirect has an invalid location containing 127.0.0.1
       then we regard the url as ok
   - ignoring .tfm files when checking for duplicates
   - now checking for UTF BOMs. If a README file contains a BOM we issue an error message,
     if other files contain BOMs we issue a warning message

2019-03-11 (1.4.0)
   - recognizing generated files in a .dtx file when they are included using a
     filecontents resp. filecontents* environment.

2019-03-14 (1.5.0)
   - a README can be a symlink which will now be detected properly
   - if a symlink points to a file object outside of the package directory tree error message
     e0030 will be issued

2019-03-29 (1.6.0)
   - new error message e0031 when a filename contains invalid UTF-8 characters

2019-06-30 (1.7.0)
   - when symlinks occur in e0025 they are reported as files
   - improved error message e0025 now displaying the paths of the affected
       files/directories/symlinks
   - if a found http(s) link ends with "`" then the trailing "`" will be discarded
   - compiled with rust edition 2018

2019-07-20 (1.8.0)
   - add a check to detect temporary files in the TDS zip archive

2019-07-21 (1.8.1)
   - correct typo in e0008 message text

2019-08-07 (1.8.2)
   - updating crates, requiring slight code changes in integration.rs
   - ignore windows files .nsh and .reg when checking for LF line endings
   - new error message e0034 for unwanted files in top level directory of a TDS zip archive
   - e0020 now reporting only unwanted directory in top level directory of a TDS zip archive

2019-10-01 (1.8.3)
   - (Experimental) issue warning if a file in the package tree (W0005) or in the TDS zip archive (W0006) is
     larger than 40MiB
   - (Experimental) for babel-base.tds.zip use the real package name 'babel' (instead of 'babel-base') in order
     to check if the path name contains the package name

     Both features are regarded as experimental because both contain a hard coded check. If required
     things will be changed in the future.

2019-11-04 (1.9.0)
   - (Experimental) for latex-base, latex-tools, latex-graphics and latex-amsmath use the real
     directory name 'latex' when checking path names in the TDS zip archive
   - New error message e0035 if a TDS zip archive is found in the unpacked directory tree
   - add testcases for w0005, w0006 and e0035

2019-11-27 (1.9.1)
   - recognize .bat files as script type
   - add test case for .bat file recognition

2019-11-28 (1.10.0)
   - remove message E0020 to report unwanted top level directory in a TDS zip archive
   - more checks for a TDS zip archive
     - check for temporary files or directories

2020-01-17 (2.0.0)
   - more error checking in TDS zip archives
     - detecting if a .dtx or .ins file is not in a subdirectory of source/ or doc/ toplevel directories (E0036)
     - detecting empty directories (W0007)
     - detecting hidden directories (E0006)
     - detecting temporary files (E0008)
       - as a special case tex/latex/tools/.tex is allowed for package latex-tools. The
         same applies for latex-tools-dev package
   - revamped handling of line ending checks
     - reporting if a file has CR line endings only (E0037)
     - reporting if a file has inconsistent line endings (E0038)
     - reporting if a Windows file does not have Windows (CRLF) line endings (W0008)
       - a file is regarded a Windows file if its name ends with one of .bat, .cmd, .nsh or .reg
     - files with CR, or CRLF line endings will be corrected to LF
     - but windows files will be corrected to CRLF line endings
     - when correcting line endings a line ending at the end of a file will be added if required
     - renamed command line switch
       - old: -L, --correct-crlf         Correct CRLF line endings
       - new: -L, --correct-le           Correct line endings
   - other changes
     - new message F0007 when tmp directory is not writable or the tmp directory is full
     - some code simplifications
     - crates update
     - when detecting an empty file also permissions will be checked and (if requested) corrected
     - (Experimental) for latex-base-dev, latex-tools-dev, latex-graphics-dev and latex-amsmath-dev
       check for latex-dev in the path names in the TDS zip archive
     - if a file in a generated file statement contains a directory part it was correctly
       recognized but not compared correctly to the contents of the TDS zip archive

2020-02-07 (2.0.1)
   - use the `-a` (convert text files) flag when temporarily unzipping the TDS archive
   - crates update

2020-04-12 (2.1.0)
   - add new test case e0033
   - don't issue error messages when a .bat file has 0755 permission
   - improvements regarding detection of generated files (error message e0019)
     - generated REAME files caused e0019 error message when they were specified with
       a relative path name in the `\generate` statement in the .dtx resp. .ins file
     - don't issue error message when a pdf file, generated by .dtx/.ins, exists
     - don't issue error message when an existing .ins file is generated by a .dtx file
     - improved text in error message e0019
   - call pdfinfo and unzip without specifying an absolute path so that it gets found
     when it is outside of /usr/bin (reported by Arthur Reutenauer)

2020-11-20 (2.2.0)
   - small code changes due to new release of blake2 library
   - replace deprecated tempdir crate by tempfile crate
   - (Experimental) instead of latex-firstaid use latex/firstaid when checking path names
     in TDS zip archive
   - unpack TDS zip without -a flag which was introduced in 2.0.1. According to Petra this creates
       more problems than it solves
   - revive e0020 which checks for unwanted directories in the top level directory of a TDS zip
       archive. 
   - new error message e0039: no doc/ top level directory in tds zip archive
   - new error message e0040: too few top level directories in tds zip archive
   - new error message e0041: map file found in path not starting with `fonts/map/dvips`
   - add more test cases
2021-mm-dd (2.2.1)
   - if a URL ends with '。', which is an ideographic full stop (U+3002), remove that character
