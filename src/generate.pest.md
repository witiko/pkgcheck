# Some notes about generate.pest

## COMMENT and WHITESPACE

The Pest book at https://pest.rs/book/grammars/syntax.html says:

> Many languages and text formats allow arbitrary whitespace and comments between logical tokens. 
> For instance, Rust considers 4+5 equivalent to 4 + 5 and 4 /* comment */ + 5.

> The optional rules WHITESPACE and COMMENT implement this behaviour. If either (or both) are defined, 
> they will be implicitly inserted at every sequence and between every repetition (except in atomic rules).


Therefore COMMENT and WHITESPACE are defined in `generate.pest`.

